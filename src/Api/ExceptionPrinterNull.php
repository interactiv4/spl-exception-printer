<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Exception\Printer\Api;

use Interactiv4\Contracts\SPL\Exception\Printer\Api\ExceptionPrinterInterface;
use Interactiv4\Contracts\SPL\Exception\Printer\Api\ExceptionPrinterNullTrait;

/**
 * Class ExceptionPrinterNull.
 *
 * Use this class to help yourself to implement ExceptionPrinterInterface.
 * It swallows the exception without printing anything.
 *
 * @see ExceptionPrinterInterface
 *
 * @api
 *
 * @package Interactiv4\SPL\Exception\Printer
 */
class ExceptionPrinterNull implements ExceptionPrinterInterface
{
    use ExceptionPrinterNullTrait;
}
