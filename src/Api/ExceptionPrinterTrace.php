<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Exception\Printer\Api;

use Interactiv4\Contracts\SPL\Exception\Printer\Api\ExceptionPrinterTraceTrait;
use Interactiv4\Contracts\SPL\Exception\Printer\Api\ExceptionPrinterInterface;

/**
 * Class ExceptionPrinterTrace.
 *
 * Use this class to help yourself to implement ExceptionPrinterInterface.
 * It prints a exception message and trace and their previous exceptions ones to stdout.
 *
 * @see ExceptionPrinterInterface
 *
 * @api
 *
 * @package Interactiv4\SPL\Exception\Printer
 */
class ExceptionPrinterTrace implements ExceptionPrinterInterface
{
    use ExceptionPrinterTraceTrait;
}
